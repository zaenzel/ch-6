import React from "react";
import { Grid, Box, Typography, Stack, Container } from "@mui/material";
import Link from "@mui/material/Link";
import { FaFacebook, FaInstagram, FaTwitter, FaTwitch } from "react-icons/fa";
import { AiOutlineMail } from "react-icons/ai";

const Footer = () => {
  return (
    <>
      <Container>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            flexGrow: "1",
            mt: 10
          }}
        >
          <Stack
            direction={{ xs: "column", sm: "column", md: "row" }}
            spacing={{ xs: 5, sm: 5, md: 10 }}
            margin={5}
          >
            <Box>
              <Stack spacing={2}>
                <Typography variant="body">
                  Jalan Suryono No. 161 Mayangan Kota Probolonggo 672000
                </Typography>
                <Typography variant="body">binarcarrental@gmail.com</Typography>
                <Typography variant="body">081-233-334=808</Typography>
              </Stack>
            </Box>
            <Box
              sx={{
                typography: "body1",
                "& > :not(style) + :not(style)": {},
              }}
            >
              <Stack spacing={2}>
                <Link href="#" underline="none" color="inherit" variant="body">
                  {"Our Service"}
                </Link>
                <Link href="#" underline="none" color="inherit" variant="body">
                  {"Why Us"}
                </Link>
                <Link href="#" underline="none" color="inherit" variant="body">
                  {"Testimonial"}
                </Link>
                <Link href="#" underline="none" color="inherit" variant="body">
                  {"FAQ"}
                </Link>
              </Stack>
            </Box>
            <Box>
              <Typography variant="body">Connect with us</Typography>
              <Stack direction="row" spacing={2} mt={2}>
                <Link href="#" underline="none" color="inherit">
                  <FaFacebook />
                </Link>
                <Link href="#" underline="none" color="inherit">
                  <FaInstagram />
                </Link>
                <Link href="#" underline="none" color="inherit">
                  <FaTwitter />
                </Link>
                <Link href="#" underline="none" color="inherit">
                  <AiOutlineMail />
                </Link>
                <Link href="#" underline="none" color="inherit">
                  <FaTwitch />
                </Link>
              </Stack>
            </Box>
            <Box sx={{}}>
              {" "}
              <Typography variant="body">Copyright Binar 2022</Typography>
            </Box>
          </Stack>

          <Grid item md={3} xs={12}></Grid>
          <Grid item md={3} xs={12}></Grid>
          <Grid item md={3} xs={12}></Grid>
          <Grid item md={3} xs={12}></Grid>
        </Box>
      </Container>
    </>
  );
};

export default Footer;
