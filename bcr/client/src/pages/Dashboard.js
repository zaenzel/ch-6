import { Grid, Stack } from "@mui/material";
import React from "react";
import { Navigate } from "react-router-dom";
import Navbar from "../componentAdmin/Navbar";
import DataTable from "../componentAdmin/DataTable";
import SideBar from "../componentAdmin/SideBar";

const Dashboard = () => {

  return (
    <>
      <Stack direction='row' justifyContent='space-between'>
        <SideBar />
        <DataTable />
      </Stack>

    </>
  );
};

export default Dashboard;
