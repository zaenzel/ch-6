import React, { useContext, useState } from "react";
import { styled, alpha } from "@mui/material/styles";
import { Avatar, Stack } from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Badge from "@mui/material/Badge";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import { data } from "../App";
import DirectionsCarFilledIcon from "@mui/icons-material/DirectionsCarFilled";
import MailIcon from "@mui/icons-material/Mail";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import FormatAlignJustifyIcon from "@mui/icons-material/FormatAlignJustify";

export default function SearchAppBar() {
  // const { user, setUser } = useContext(data);

  const StyledToolbar = styled(Toolbar)(({ theme }) => ({
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      gap: 15,
    },
    backgroundColor: "white",
  }));

  const Search = styled("div")(({ theme }) => ({
    backgroundColor: "white",
    padding: "0 10px",
    border: "1px solid #D0D0D0",
    borderRadius: theme.shape.borderRadius,
  }));

  const Icons = styled(Box)(({ theme }) => ({
    display: "none",
    alignItems: "center",
    gap: "20px",
    [theme.breakpoints.up("sm")]: {
      display: "flex",
    },
  }));

  const UserBox = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    gap: "10px",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  }));

  const [open, setOpen] = useState(false);

  return (
    <AppBar position="sticky">
      <StyledToolbar>
        <Stack direction="row" spacing={2}>
          <FormatAlignJustifyIcon htmlColor="black" />
          <Typography
            color="black"
            sx={{ display: { xs: "none", sm: "block" } }}
          >
            Admin BCR
          </Typography>
          <DirectionsCarFilledIcon
            sx={{ display: { xs: "block", sm: "none" } }}
          />
        </Stack>
        <Search>
          <InputBase placeholder="search..." />
        </Search>

        <Icons>
          <Badge badgeContent={4} color="error">
            <MailIcon htmlColor="black" />
          </Badge>
          {/* user.hasOwnProperty("photos") ? user.photos[0].value :  */}
          <Avatar
            alt="Admin img"
            src={null}
            sx={{ width: 30, height: 30 }}
            onClick={(e) => setOpen(true)}
          />
          <Typography variant="list" color="black">haha</Typography>
        </Icons>
        <UserBox>
          <Badge badgeContent={4} color="error">
            <MailIcon htmlColor="black" />
          </Badge>
          {/* user.hasOwnProperty("photos") ? user.photos[0].value :  */}
          <Avatar
            alt="Admin img"
            src={null}
            sx={{ width: 30, height: 30 }}
            onClick={(e) => setOpen(true)}
          />
        </UserBox>
      </StyledToolbar>
      <Menu
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        open={open}
        onClose={(e) => setOpen(false)}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <MenuItem>Profile</MenuItem>
        <MenuItem>My account</MenuItem>
        <MenuItem>Logout</MenuItem>
      </Menu>
    </AppBar>
  );
}
