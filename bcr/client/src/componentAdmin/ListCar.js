import { Stack } from "@mui/material";
import React from "react";
import DataList from "./DataList";
import SideBar from "./SideBar";

const ListCar = () => {
  return (
    <Stack direction="row" justifyContent="space-between">
      <SideBar />
      <DataList />
    </Stack>
  );
};

export default ListCar;
