import { Box, Container, Typography } from "@mui/material";
import React, { Children } from "react";
import CarCard from "../component/CarCard";
import Navbar from "./Navbar";
import { useSelector, useDispatch } from "react-redux";
import LinearProgress from "@mui/material/LinearProgress";

import { getAllCars } from "../redux/carSlice";
import CarListing from "../component/CarListing";
import Search from "../component/Search";

const DataList = () => {
  const cars = useSelector(getAllCars);
  console.log(cars);
  return (
    <>
      <Box flex={11}>
        {/* <Navbar /> */}
        <Search />
        <CarListing />
      </Box>
    </>
  );
};

export default DataList;
