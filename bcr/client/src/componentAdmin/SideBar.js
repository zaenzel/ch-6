import {
  Box,
  Grid,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import DirectionsCarFilledIcon from "@mui/icons-material/DirectionsCarFilled";
import RectangleIcon from "@mui/icons-material/Rectangle";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const SideBar = () => {
  const StyledSidebar = styled(Box)(({ theme }) => ({
    flex: 1,
    display: "block",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
    backgroundColor: theme.palette.primary.main,
    height: "fullHeight",
  }));

  const Icons = styled(Grid)(({ theme }) => ({
    gap: "5px",
    color: "white",
    padding: 20,
    direction: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  }));

  return (
    <StyledSidebar>
        <Grid container sx={{ justifyContent: "center", marginTop: 3 }}>
          <RectangleIcon fontSize="large" htmlColor="white" />
          <Link to="/dashboard">
            <Icons container component="a" href="#dashboard">
              <HomeOutlinedIcon fontSize="large" />
              <Typography variant="body">Dashboard</Typography>
            </Icons>
          </Link>
          <Link to="/dashboard/listcar">
            <Icons container component="a" href="#list-car">
              <DirectionsCarFilledIcon fontSize="large" />
              <Typography variant="body">List Car</Typography>
            </Icons>
          </Link>
        </Grid>
    </StyledSidebar>
  );
};

export default SideBar;
