import React, { createContext, useEffect, useState } from "react";
import "./App.css";
import { Navigate, Route, Routes } from "react-router-dom";
import Main from "./pages/Main";
import Detail from "./pages/Detail";
import Page404 from "./pages/Page404";
import Login from "./pages/Login";
import Payment from "./pages/Payment";
import Dashboard from "./pages/Dashboard";
export const data = createContext();

function App() {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getUser = () => {
      fetch("http://localhost:5000/auth/login/success", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          setLoading(false);
          console.log(loading);
          throw new Error("Authentication failed");
        })
        .then((response) => {
          setUser(response.user);
          setLoading(false);
          console.log(loading);
          console.log(response.user);
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
          console.log(loading);
        });
    };
    getUser();
  }, []);

  return (
    <div className="App">
      <data.Provider value={{ user, setUser }}>
        <Routes>
          <Route path="/" element={<Main />} />
          <Route
            path="/login"
            element={user ? user.isAdmin ? <Navigate to="/dashboard" /> : <Navigate to="/" /> : <Login />}
          />
          <Route
            path="/dashboard"
            element={user ? <Dashboard /> : <Navigate to="/login" />}
          />
          <Route path="/detail/:id" element={<Detail />} />
          <Route
            path="/payment/:id"
            element={user ? <Payment /> : <Navigate to="/Login" />}
          />
          <Route path="*" element={<Page404 />} />
        </Routes>
      </data.Provider>
    </div>
  );
}

export default App;
