import { Grid } from "@mui/material";
import React from "react";
import { Navigate } from "react-router-dom";
import ContentAdmin from "../componentAdmin/ContentAdmin";
import SideBar from "../componentAdmin/SideBar";

const Dashboard = () => {

  const token = localStorage.getItem("token");
  const email = localStorage.getItem("email");
  const isAdmin = localStorage.getItem("isAdmin");
  console.log(isAdmin);

  return (
    <>
      <Grid container>
        <Grid item md={2}>
          <SideBar />
        </Grid>
        <Grid item md={10}>
          <ContentAdmin />
        </Grid>
      </Grid>
    </>
  );
};

export default Dashboard;
