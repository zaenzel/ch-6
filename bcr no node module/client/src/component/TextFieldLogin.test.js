import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import TextFieldLogin from "./TextFieldLogin";
import "@testing-library/jest-dom";
import { BrowserRouter } from 'react-router-dom';

test("ini string", () => {
  render(<TextFieldLogin />, { wrapper: BrowserRouter });
  const welcome = screen.getByText(/Welcome Admin BCR/i);
  expect(welcome).toBeInTheDocument();
});

test("username input should be rendered", () => {
  render(<TextFieldLogin />, { wrapper: BrowserRouter })
  const email = screen.getByRole("dialog");
  expect(email).toBeInTheDocument();
});

test("password input should be rendered", () => {
  render(<TextFieldLogin />, { wrapper: BrowserRouter })
  const pass = screen.getByRole("dialogPass");
  expect(pass).toBeInTheDocument();
});

test("button should be rendered", () => {
  render(<TextFieldLogin />, { wrapper: BrowserRouter });
  const buttonEl = screen.getByRole("buttonSignin");
  expect(buttonEl).toBeInTheDocument();
});
