import React from "react";
import {
  Paper,
  Stack,
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import { MdOutlineExpandMore } from "react-icons/md";

const ListDetail = () => {
  return (
    <>
      <Paper elevation={2}>
        <Stack p={5} spacing={3}>
          <Typography variant="title">Tentang Paket</Typography>
          <Typography variant="title" fontWeight="normal">
            Include
          </Typography>
          <Stack item spacing={1}>
            <li>
              <Typography variant="list">
                Apa saja yang termasuk dalam paket misal durasi max 12 jam{" "}
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Sudah termasuk bensin selama 12 jam{" "}
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Sudah termasuk Tiket Wisata
              </Typography>
            </li>
            <li>
              <Typography variant="list">Sudah termasuk pajak</Typography>
            </li>
          </Stack>
          <Typography variant="title" fontWeight="normal">
            Exclude
          </Typography>

          <Stack item spacing={1}>
            <li>
              <Typography variant="list">
                Tidak termasuk biaya makan sopir Rp 75.000/hari
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                20.000/jam
              </Typography>{" "}
            </li>
            <li>
              <Typography variant="list">
                Sudah termasuk Tiket Wisata
              </Typography>{" "}
            </li>
            <li>
              <Typography variant="list">
                Tidak termasuk akomodasi penginapan
              </Typography>
            </li>
          </Stack>

          {/*
          <Typography variant="title" fontWeight="normal">
            Refund, Reschedule, Overtime
          </Typography>
          <Stack item spacing={1}>
            <li>
              <Typography variant="list">
                Tidak termasuk biaya makan sopir Rp 75.000/hari
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                20.000/jam
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Tidak termasuk akomodasi penginapan
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Tidak termasuk biaya makan sopir Rp 75.000/hari
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                20.000/jam
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Tidak termasuk akomodasi penginapan
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Tidak termasuk biaya makan sopir Rp 75.000/hari
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                20.000/jam
              </Typography>
            </li>
            <li>
              <Typography variant="list">
                Tidak termasuk akomodasi penginapan
              </Typography>
            </li>
          </Stack>
  */}

          <Accordion elevation={0} sx={{ border: "0" }}>
            <AccordionSummary
              expandIcon={<MdOutlineExpandMore />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography
                variant="title"
                component="div"
                sx={{ fontWeight: "normal" }}
              >
                Refund, Reschedule, Overtime
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Stack item spacing={1}>
                <li>
                  <Typography variant="list">
                    Tidak termasuk biaya makan sopir Rp 75.000/hari
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                    20.000/jam
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Tidak termasuk akomodasi penginapan
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Tidak termasuk biaya makan sopir Rp 75.000/hari
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                    20.000/jam
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Tidak termasuk akomodasi penginapan
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Tidak termasuk biaya makan sopir Rp 75.000/hari
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
                    20.000/jam
                  </Typography>
                </li>
                <li>
                  <Typography variant="list">
                    Tidak termasuk akomodasi penginapan
                  </Typography>
                </li>
              </Stack>
            </AccordionDetails>
          </Accordion>
        </Stack>
      </Paper>
    </>
  );
};

export default ListDetail;
