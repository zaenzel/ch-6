import React from "react";
import { Grid, TextField, Typography } from "@mui/material";

const TextFieldSearch = () => {
  return (
    <>
      <Grid
        container
        p={3}
        spacing={3}
        sx={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            Tipe Mobil
          </Typography>
          <TextField
            id="mobil"
            name="mobil"
            variant="filled"
            disabled
            // onChange={(e) => setMobil(e.target.value)}
          />
        </Grid>
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            Pilih Tanggal
          </Typography>
          <TextField disabled variant="filled" />
        </Grid>
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            waktu jemput
          </Typography>
          <TextField
            disabled
            id="mobil"
            variant="filled"
            // onChange={(e) => setMobil(e.target.value)}
          />
        </Grid>
        <Grid
          item
          direction="column"
          sx={{
            display: "flex",
          }}
        >
          <Typography sx={{ mb: 2 }} variant="body">
            jumlah penumpang
          </Typography>
          <TextField
            disabled
            id="mobil"
            variant="filled"
            // onChange={(e) => setMobil(e.target.value)}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default TextFieldSearch;
