import React, { useEffect, useState, useContext } from "react";
import {
  Container,
  Grid,
  TextField,
  Typography,
  Box,
  IconButton,
  Stack,
  Link,
} from "@mui/material";
import Button from "./Button";
import { useDispatch, useSelector } from "react-redux";
import { getBtn, setButton } from "../redux/carSlice";
import { useNavigate } from "react-router-dom";

import { FcGoogle } from "react-icons/fc";
import { FaGithub } from "react-icons/fa";
import axios from "axios";
import { data } from "../App";

const TextFieldLogin = () => {
  // const btn = useSelector(getBtn);
  // const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(setButton(  "Login"));
  // }, []);
  // const navigate = useNavigate();

  const navigate = useNavigate()

  const google = () => {
    window.open("http://localhost:5000/auth/google", "_self");
  };

  const github = () => {
    window.open("http://localhost:5000/auth/github", "_self");
  };

  const [register, setRegister] = useState(false);
  const {user, setUser} = useContext(data);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onsubmit = async (e) => {
    console.log(user);
    e.preventDefault();
    try {
      const res = await axios.post("http://localhost:5000/auth/login", {
        email,
        password,
      });
      setUser(res.data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <Box sx={{ m: 5 }} validate>
        <Typography component="h1" variant="h6" sx={{ mb: 5 }}>
          Welcome Admin BCR
        </Typography>
        <Typography>Email</Typography>
        <TextField
          id="email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          variant="outlined"
          fullWidth
          type="email"
          role="dialog"
        />
        <Typography sx={{ mt: 3 }}>Password</Typography>
        <TextField
          id="password"
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          variant="outlined"
          fullWidth
          type="password"
          role="dialogPass"
        />
        <Button
          type="submit"
          fullWidth
          sx={{ mt: 3, mb: 2 }}
          onClick={onsubmit}
          role="buttonSignin"
        >
          {register ? `Sign up` : "Login"}
        </Button>
        <Stack
          spacing={2}
          sx={{ display: "flex", alignItems: "center", mt: 2 }}
        >
          <Typography variant="body2" color="gray">
            {register ? `or sign up using` : `or login using`}
          </Typography>
          <Box>
            <IconButton onClick={google}>
              <FcGoogle />
            </IconButton>
            <IconButton onClick={github}>
              <FaGithub />
            </IconButton>
          </Box>

          {register ? (
            <Box sx={{ display: "flex" }}>
              <Typography variant="body2" color="gray" mr={1}>
                Have already account?
              </Typography>
              <Link
                component="button"
                onClick={() => {
                  setRegister(false);
                }}
              >
                LOGIN
              </Link>
            </Box>
          ) : (
            <Box sx={{ display: "flex" }}>
              <Typography variant="body2" color="gray" mr={1}>
                Need an account?
              </Typography>
              <Link
                component="button"
                onClick={() => {
                  setRegister(true);
                }}
              >
                SIGN UP
              </Link>
            </Box>
          )}
        </Stack>
      </Box>
    </>
  );
};

export default TextFieldLogin;
