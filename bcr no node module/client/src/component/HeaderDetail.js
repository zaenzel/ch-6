import { Box } from "@mui/material";
import React from "react";
import Navbar from "./Navbar";

const Header = () => {
  return (
    <>
      <Box
        sx={{
          backgroundColor: "#F1F3FF",
          width: "100v%",
          height: "200px",
        }}
      >
        <Navbar />
      </Box>
    </>
  );
};

export default Header;
