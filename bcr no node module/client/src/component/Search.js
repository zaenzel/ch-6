import React from "react";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { Box, Grid, Paper, Stack, TextField, Typography } from "@mui/material";
import TextFieldSearch from "./TextFieldSearch";

const Search = () => {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          flexGrow: "1",
          fontFamily: "Helvetica Neue",
          transform: "translateY(-50%)",
          margin: 5,
        }}
      >
        <Paper>
          <Stack container>
            <Typography variant="title" ml={4} pt={3}>
              Pencarianmu
            </Typography>
            <Stack
              container
              direction={{ xs: "column", sm: "column", md: "row" }}
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <TextFieldSearch />
            </Stack>
          </Stack>
        </Paper>
      </Box>
    </>
  );
};

export default Search;
