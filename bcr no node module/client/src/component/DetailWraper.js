import React from "react";
import CarCardDetail from "./CarCardDetail";
import ListDetail from "./ListDetail";
import { Container, Grid } from "@mui/material";
import { Box } from "@mui/system";
import Button from "./Button";

import { useDispatch, useSelector } from "react-redux";
import { getBtn, setButton } from "../redux/carSlice";
import { useNavigate, useParams } from "react-router-dom";

const DetailWraper = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const btn = useSelector(getBtn);
  const dispatch = useDispatch();
  dispatch(setButton("Lanjutkan Pembayaran"));

  return (
    <>
      <Container>
        <Grid
          container
          spacing={{ xs: 3, sm: 0 }}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <Grid item md={8}>
            <ListDetail />
            <Box
              mt={5}
              mb={5}
              sx={{
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button onClick={() => navigate(`/payment/${id}`)}>{btn}</Button>
            </Box>
          </Grid>
          <Grid item md={4}>
            <CarCardDetail id={id}/>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default DetailWraper;
